import Table from "./datatable.js";
import { columns, data } from "./datainput.js";

const table = new Table({
  columns: [...columns],
  data: [...data],
});

const app = document.getElementById("app");
table.render(app);
