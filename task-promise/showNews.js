import { addNews } from "./addNews.js";

let berita = [];

const searchLabel = document.getElementById("basic-addon1");
//cari berita
searchLabel.addEventListener("keyup", (show) => {
  const searchNews = show.target.value.toLowerCase();

  const filterNews = berita.filter((b) => {
    return b.title.toLowerCase().includes(searchNews);
  });
  console.log(searchNews);
  addNews(filterNews);
});

//fetch data dari newsapi
// const getData = async () => {
//   try {
//     const showApi = await axios.get(
//       "https://newsapi.org/v2/top-headlines?country=id&apiKey=2ce3391dc00c4c4395e8ba75f6384d25"
//     );
//     berita = showApi.data.articles;
//     addNews(berita);
//   } catch (error) {
//     console.log(error.message);
//   }
// };

// const getData = require("axios").default;

axios
  .get(
    "https://newsapi.org/v2/top-headlines?country=id&apiKey=2ce3391dc00c4c4395e8ba75f6384d25"
  )
  .then((resp) => {
    berita = resp.data.articles;
    addNews(berita);
    console.log(resp.data);
  })
  .catch((err) => {
    // Handle Error Here
    console.error(err);
  });

//mengambil fungsi getData
// getData();
