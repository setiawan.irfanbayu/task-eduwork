//Menampilkan data

const cards = document.getElementById("card-container");
export const addNews = (berita) => {
  const card = berita
    .map((b) => {
      return `
          <div class="col-12 mb-3">
            <div class="card d-flex flex-md-row">
                <img src="${b.urlToImage}" class="card-img-top img-fluid" alt="..."  />
                <div class="card-body px-md-5">
                  <h5 class="card-title">${b.title}</h5>
                  <p class="card-text mt-3">${b.content}</p>
                  <a href="${b.url}" class="btn btn-primary">Read More..</a>
                </div>
            </div>
          </div>
        `;
    })
    .join("");

  cards.innerHTML = card;
};

// ubah 1
