$(document).ready(function () {
  // FETCHING DATA FROM JSON FILE
  $.getJSON("https://jsonplaceholder.typicode.com/users/", function (data) {
    var employee = "";

    // ITERATING THROUGH OBJECTS
    $.each(data, function (key, value) {
      //CONSTRUCTION OF ROWS HAVING
      // DATA FROM JSON OBJECT
      employee += "<tr>";
      employee += "<td>" + value.id + "</td>";

      employee += "<td>" + value.name + "</td>";

      employee += "<td>" + value.username + "</td>";

      employee += "<td>" + value.email + "</td>";

      employee +=
        "<td>" + value.address.street + "," + value.address.suite + ",";
      value.address.city + "</td>";

      employee += "<td>" + value.company.name + "</td>";

      employee += "</employee>";
    });

    //INSERTING ROWS INTO TABLE
    $("#example").append(employee);
  });
  (error) => {
    console.log(error.message);
  };
});
